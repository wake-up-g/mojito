# Mojito 服务端

## 环境要求
- Node.js >= 14.0.0
- MongoDB >= 4.2.12

## 开发
``` bash
npm run dev
```
## 生产
``` bash
npm run build
npm start
```